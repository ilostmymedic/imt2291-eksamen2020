<?php
    require_once ('../classes/DB.php');
    

    function register($email, $pass, $firstname, $lastname) {
        $error = false;
        $errorMessage = '';
        $success = false;
        $user = [];

        if($error === false) {
            $db = DB::getDBConnection();

            $sql = 'INSERT INTO user (uname, pwd, firstName, lastName, avatar) VALUES (?,?,?,?,?)';
            $stmt = $db->prepare($sql);
            $stmt->execute(array($email, password_hash($pass, PASSWORD_DEFAULT), $firstname, $lastname, NULL));

            $success = true;

            echo json_encode([
                'result' => $success
            ]);
        }
    }
    
    if(
        isset($_POST['email']) ||
        isset($_POST['pass']) ||
        isset($_POST['firstname']) ||
        isset($_POST['lastname'])
    ) {
    
        register($_POST['email'], $_POST['pass'], $_POST['firstname'], $_POST['lastname'],);
    } else {
        echo json_encode([
            'result' => $success
        ]);
    }